import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    WebDriver webDriver;
    public static final String urlLogin = "http://localhost:8080/wordpress/wp-login.php";
    public static final String urlDashboard = "http://localhost:8080/wordpress/wp-admin/";

    // Chạy trước các test case: Khởi tạo driver object sau đó truy cập vào trang login
    // (Hầu hết các test case sẽ làm việc với trang login đầu tiên)
    @BeforeMethod
    public void setUp() {
        webDriver = new ChromeDriver();
        webDriver.get(urlLogin);
    }

    // Chạy sau các test case: Thoát trình duyệt
    @AfterMethod
    public void tearDown() {
        webDriver.quit();
    }
}
