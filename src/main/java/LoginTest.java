import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/*
DucPM - 20/09/21
Sử dụng Selenium WebDriver để test trang đăng nhập Wordpress
 */

public class LoginTest extends BaseTest {

    String idUserLogin = "user_login";
    String idUserPassword = "user_pass";
    String idSubmitLoginButton = "wp-submit";

    public void login(String userName, String password) {
        // Điền thông tin username, password và click vào button submit
        // senKeys() và click() là method của interface WebElement
        webDriver.findElement(By.id(idUserLogin)).sendKeys(userName);
        webDriver.findElement(By.id(idUserPassword)).sendKeys(password);
        webDriver.findElement(By.id(idSubmitLoginButton)).click();
    }

    // Test case: Đăng nhập vào dashboard thành công
    @Test
    public void loginByAdmin() {
        login("admin", "admin");
        // Kiểm tra xem có login thành công hay không: chuyển đến trang chủ => pass
        Assert.assertEquals(webDriver.getCurrentUrl(), BaseTest.urlDashboard);
    }

    // Test case: Bỏ trống username và password để đăng nhập
    @Test
    public void loginByBlankAccount() {
        login("","");
        // Kiểm tra xem đăng nhập không thành công thì có giữ nguyên url không => pass
        Assert.assertEquals(webDriver.getCurrentUrl(), BaseTest.urlLogin);
    }
}
