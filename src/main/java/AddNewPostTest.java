import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddNewPostTest extends BaseTest{

    String idUserLogin = "user_login";
    String idUserPassword = "user_pass";
    String idSubmitLoginButton = "wp-submit";

    public void login(String userName, String password) {
        // Điền thông tin username, password và click vào button submit
        // senKeys() và click() là method của interface WebElement
        webDriver.findElement(By.id(idUserLogin)).sendKeys(userName);
        webDriver.findElement(By.id(idUserPassword)).sendKeys(password);
        webDriver.findElement(By.id(idSubmitLoginButton)).click();
    }

    @Test
    public void addNewPost() {
        Actions actions = new Actions(webDriver);
        webDriver.get("http://localhost:8080/wordpress/wp-admin/post-new.php");
        login("admin", "admin");
        webDriver.findElement(By.id("title")).sendKeys("This is the title");
        webDriver.switchTo().frame("content_ifr");
        webDriver.findElement(By.id("tinymce")).sendKeys("This is the body");
        webDriver.switchTo().defaultContent();
        webDriver.findElement(By.id("publish")).click();
        webDriver.findElement(By.cssSelector("#message a")).click();
        String title = webDriver.findElement(By.tagName("h1")).getText();
        Assert.assertEquals(title, "This is the title");

    }
}
